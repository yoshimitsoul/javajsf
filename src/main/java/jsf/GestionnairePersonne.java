package jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class GestionnairePersonne {

	private Personne personne = new Personne();
	
	public Personne getPersonne() {
		return personne;
	}
	
	public String accueillir() {
		System.out.println("appel de la methode accueillir");
		System.out.println(personne.getNom());
		return null;
	}
}
