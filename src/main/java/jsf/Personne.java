package jsf;

public class Personne {
	
	private String nom;
	private int age;
	
	public String getNom() {
		return nom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
